package mavenTableProject;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SimpleTableListener implements TaskListener {
	
	private static final long serialVersionUID = 1L;

	private static final String ASSIGNEE = "assignee";
	private static final String SELECTED_ID = "id";
	private static final String SELECTED_NAME = "name";

	private static Log log = LogFactory.getLog(SimpleTableListener.class);

	public Expression variableNameExpression;

	@SuppressWarnings("unchecked")
	public void notify(DelegateTask delegateTask) {
		log.info("gathering opinions...");

		List<String> selectedFacts = new ArrayList<String>();

		if (delegateTask.hasVariable(SELECTED_NAME)) {
			selectedFacts = (List<String>) delegateTask.getVariable(SELECTED_NAME);
		}

		String id = delegateTask.getVariable(SELECTED_ID, String.class);
		String name = delegateTask.getVariable(ASSIGNEE, String.class);

		log.info(id + ": " + name);

		selectedFacts.add(id + ": " + name);

		delegateTask.setVariable(SELECTED_NAME, selectedFacts);
	}

}
