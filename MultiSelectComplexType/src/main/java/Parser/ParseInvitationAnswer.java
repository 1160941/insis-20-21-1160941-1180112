package Parser;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import dto.JSONTeacherDTO;

public class ParseInvitationAnswer implements JavaDelegate{

	private static final String ADVISOR_LIST = "advisorList";
	private static final String EVERYONE_ACCEPTED = "everyoneAccepted";
	private static final String ACCEPTED_REVISOR_LIST = "acceptedRevisorList";
	private static final String REVISOR_REJECTED_NAMES = "revisorRejectNames";
	
	private static final int ADVISOR_NAME_INDEX = 0;
	private static final int ADVISOR_ANSWER_INDEX = 1;
	
	private static Log log = LogFactory.getLog(ParseInvitationAnswer.class);

	private static final long serialVersionUID = 555563281211829392L;
	
	@SuppressWarnings("unchecked")
	public void execute(DelegateExecution delegateTask) throws Exception {
		log.info("Parsing reviewers invitation decision... ");

		List<String> advisorAcceptedList = (List<String>) delegateTask.getVariable(ADVISOR_LIST);
		List<String> advisorYesList = new ArrayList();
		
		if(delegateTask.hasVariable(ACCEPTED_REVISOR_LIST)) {
			advisorYesList = (List<String>) delegateTask.getVariable(ACCEPTED_REVISOR_LIST);
		}
		
		String advisorRejectNames = "";
		
		for(String advisorWithAnswer: advisorAcceptedList) {
			String[] advisorResultList = advisorWithAnswer.split(":");
			
			if(advisorResultList[ADVISOR_ANSWER_INDEX].equals("no")) {
				advisorRejectNames = advisorRejectNames + ", " + advisorResultList[ADVISOR_NAME_INDEX];
			}else {
				// answer is yes
				advisorYesList.add(advisorResultList[ADVISOR_NAME_INDEX]);
			}
		}
		
		if(advisorRejectNames.isEmpty()) { //No advisor rejected
			delegateTask.setVariable(EVERYONE_ACCEPTED, "true");
			delegateTask.setVariable(ACCEPTED_REVISOR_LIST, advisorYesList);
		}else {			
			delegateTask.setVariable(REVISOR_REJECTED_NAMES, advisorRejectNames);
			delegateTask.setVariable(EVERYONE_ACCEPTED, "false");
		}
		
	}

}
