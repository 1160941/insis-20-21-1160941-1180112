package Parser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import dto.JSONComplexTypeDTO;
import dto.JSONStudentDTO;

public class ParseStudents implements JavaDelegate {
	
	private static final String RETRIEVED_STUDENTS = "studentJSON";
	private static final String LIST_STUDENTS = "studentListToEmail";
	private static Log log = LogFactory.getLog(ParseStudents.class);

	private static final long serialVersionUID = 232323281211829392L;
	
	public void execute(DelegateExecution delegateTask) throws Exception {
		log.info("Parsing students ...");
		ObjectMapper mapper = new ObjectMapper();
		try  {
			if (delegateTask.getVariable("studentJSON").toString() != null) {
				// get all students to dto
				try {
					String studentsJSONString = delegateTask.getVariable(RETRIEVED_STUDENTS).toString();
					
					List<JSONStudentDTO> allStudentsAvailable = mapper.readValue(studentsJSONString, new TypeReference<List<JSONStudentDTO>>(){});
					
					log.info("Parsing students from JSON to List completed!");
					
					List<String> emailList = getEmailList(allStudentsAvailable);
					
					log.info("List generated!");
					delegateTask.setVariable(LIST_STUDENTS, emailList);
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		catch(NullPointerException e) {
			log.info("student is null!");
		}
	}
	
	private List<String> getEmailList(List<JSONStudentDTO> allStudentsAvailable){
		List<String> list = new ArrayList();
		for(JSONStudentDTO jsonDto: allStudentsAvailable ) {
			list.add(jsonDto.getEmail());
		}
		return list;
	}

}
