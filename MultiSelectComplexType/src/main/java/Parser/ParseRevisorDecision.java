package Parser;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import dto.JSONTeacherDTO;

public class ParseRevisorDecision implements JavaDelegate{
	
	private static final String REVISOR_TOTAL_LIST = "teachersRetrieved";
	private static final String REVISOR_PARSER_VALIDATION = "revisorParserValidation";
	private static final String ACCEPTED_REVISOR_LIST = "acceptedRevisorList";
	
	private static Log log = LogFactory.getLog(ParseRevisorDecision.class);

	private static final long serialVersionUID = 12323281211829392L;
	
	public void execute(DelegateExecution delegateTask) throws Exception {
		log.info("Parsing revisors decision...");
		ObjectMapper mapper = new ObjectMapper();
		
		List<String> advisorAcceptedList = new ArrayList();
		advisorAcceptedList = (List<String>) delegateTask.getVariable(ACCEPTED_REVISOR_LIST);
		
		String teachers = delegateTask.getVariable(REVISOR_TOTAL_LIST).toString();
		log.info(teachers);
		List<JSONTeacherDTO> advisorList = mapper.readValue(teachers, new TypeReference<List<JSONTeacherDTO>>(){});
		int halfSize = advisorList.size()/2;
		
		if(advisorAcceptedList.size() > halfSize) {
			delegateTask.setVariable(REVISOR_PARSER_VALIDATION, "validProposal");
			log.info("The proposal was accepted with " +  advisorAcceptedList.size() + "yes votes");
		}else {
			delegateTask.setVariable(REVISOR_PARSER_VALIDATION, "invalidProposal");
			log.info("The proposal was rejected with " +  advisorAcceptedList.size() + "yes votes");
		}
		
	}

}
