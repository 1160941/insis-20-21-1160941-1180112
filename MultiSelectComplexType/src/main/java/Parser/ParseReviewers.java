package Parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import dto.JSONComplexTypeDTO;
import dto.JSONTeacherDTO;

public class ParseReviewers implements JavaDelegate{

	private static final String CHOOSEN_REVIEWERS = "reviewers";
	private static final String LIST_TEACHERS = "teachersRetrieved";
	private static final String LIST_EMAIL_REVIEWERS = "listEmailReviewers";
	private static Log log = LogFactory.getLog(ParseStudents.class);

	private static final long serialVersionUID = 232323281211829392L;
	
	public void execute(DelegateExecution delegateTask) throws Exception {
		
		log.info("PArsing available advisers...");
		
		ObjectMapper mapper = new ObjectMapper();
		if(delegateTask.getVariable(CHOOSEN_REVIEWERS).toString() != null) {
			try {
				String teachers = delegateTask.getVariable(LIST_TEACHERS).toString();
				log.info(teachers);
				List<JSONTeacherDTO> listTeachers = mapper.readValue(teachers, new TypeReference<List<JSONTeacherDTO>>(){});
				
				List<String> proposalReviewersId = Arrays.asList(((String) delegateTask.getVariable(CHOOSEN_REVIEWERS)).split(","));
				log.info("Choosen Advisors: ");
				log.info(proposalReviewersId.toString());
				
				List<String> reviewersList = getReviewersEmail(proposalReviewersId, listTeachers);
				
				delegateTask.setVariable(LIST_EMAIL_REVIEWERS, reviewersList);
			}catch (IOException e) {
				e.printStackTrace();
			}
			
		}else {
			log.info("There was not choosen teachers");
		}
	}
	
	public List<String> getReviewersEmail (List<String> proposalReviewersId, List<JSONTeacherDTO> listTeachers){
		List<String> list = new ArrayList();
		
		for (String reviewId : proposalReviewersId) {
			for(JSONTeacherDTO jsonDto: listTeachers ) {
				if (jsonDto.getId().equals(reviewId)) {
					list.add(jsonDto.getEmail());
				}
			}
		}
		
		return list;
	}

}
