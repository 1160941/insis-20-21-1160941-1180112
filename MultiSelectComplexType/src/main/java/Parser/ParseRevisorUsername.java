package Parser;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import dto.JSONTeacherDTO;

public class ParseRevisorUsername implements JavaDelegate{
	private static final String LIST_TEACHERS = "teachersRetrieved";
	private static final String LIST_ADVISOR_USERNAMES = "listAdvisorsUsername";
	
	
	private static Log log = LogFactory.getLog(ParseRevisorUsername.class);

	private static final long serialVersionUID = 1112323281211829392L;
	
	public void execute(DelegateExecution delegateTask) throws Exception {
		log.info("Parsing usernames...");
		
		ObjectMapper mapper = new ObjectMapper();
		String teachers = delegateTask.getVariable(LIST_TEACHERS).toString();
		log.info(teachers);
		List<JSONTeacherDTO> listTeachers = mapper.readValue(teachers, new TypeReference<List<JSONTeacherDTO>>(){});
		List<String> usernames = new ArrayList();
		
		for(JSONTeacherDTO teacher: listTeachers) {
			String [] names = teacher.getName().split(" ");
			log.info("Username:" + names[0]);
			usernames.add(names[0]);
		}
		
		delegateTask.setVariable(LIST_ADVISOR_USERNAMES, usernames);
	}
}
