package form.listener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import dto.JSONComplexTypeDTO;

public class MultiSelectListener implements TaskListener {
	private static final String SELECTED_KEYWORDS = "keywords";
	private static final String PROPOSAL_KEYWORDS_NAME = "keywordsToBeReviewed";
	
	private static final String SELECTED_TECHNOLOGIES = "technologies";
	private static final String PROPOSAL_TECHNOLOGIES_NAME = "technologiesToBeReviewed";
	
	private static final String SELECTED_APPROACHES = "approaches";
	private static final String PROPOSAL_APPROACHES_NAME = "approachesToBeReviewed";
	
	private static Log log = LogFactory.getLog(MultiSelectListener.class);
	

	private static final long serialVersionUID = 23232328293829392L;
	
//	public Expression keywords;
	
	public void notify(DelegateTask delegateTask) {
		log.info("gathering the selected choices");
		
		ObjectMapper mapper = new ObjectMapper();
		
		if (delegateTask.getVariable("keywords").toString() != null) {
			// get all the keywords
			try {
				String keywordsJSONString = delegateTask.getVariable("keywordsRetrieved").toString();
				
				List<JSONComplexTypeDTO> allKeywordsAvailable = mapper.readValue(keywordsJSONString, new TypeReference<List<JSONComplexTypeDTO>>(){});
				
				// get ids selected from array x,x,x
				List<String> proposalKeywordsId = Arrays.asList(((String) delegateTask.getVariable(SELECTED_KEYWORDS)).split(","));
				log.info("Choosen Keywords: ");
				log.info(proposalKeywordsId.toString());
				
				List<String> keywordsList = getKeywordsList(proposalKeywordsId, allKeywordsAvailable);
				
				delegateTask.setVariable(PROPOSAL_KEYWORDS_NAME, keywordsList);
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (delegateTask.getVariable("technologies").toString() != null) {
			// get all the keywords
			try {
				String technologiesJSONString = delegateTask.getVariable("technologiesRetrieved").toString();
				
				List<JSONComplexTypeDTO> allTechnologiesAvailable = mapper.readValue(technologiesJSONString, new TypeReference<List<JSONComplexTypeDTO>>(){});
				
				// get ids selected from array x,x,x
				List<String> proposalTechnologiesId = Arrays.asList(((String) delegateTask.getVariable(SELECTED_TECHNOLOGIES)).split(","));
				log.info("Choosen Keywords: ");
				log.info(proposalTechnologiesId.toString());
				
				List<String> technologiesList = getKeywordsList(proposalTechnologiesId, allTechnologiesAvailable);
				
				delegateTask.setVariable(PROPOSAL_TECHNOLOGIES_NAME, technologiesList);
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (delegateTask.getVariable("approaches").toString() != null) {
			// get all the keywords
			try {
				String approachesJSONString = delegateTask.getVariable("approachesRetrieved").toString();
				
				List<JSONComplexTypeDTO> allApproachesAvailable = mapper.readValue(approachesJSONString, new TypeReference<List<JSONComplexTypeDTO>>(){});
				
				// get ids selected from array x,x,x
				List<String> proposalApproachesId = Arrays.asList(((String) delegateTask.getVariable(SELECTED_APPROACHES)).split(","));
				log.info("Choosen Keywords: ");
				log.info(proposalApproachesId.toString());
				
				List<String> approachesList = getKeywordsList(proposalApproachesId, allApproachesAvailable);
				
				delegateTask.setVariable(PROPOSAL_APPROACHES_NAME, approachesList);
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public List<String> getKeywordsList(List<String> proposalKeywordsId, List<JSONComplexTypeDTO> allKeywordsAvailable){
		List<String> list = new ArrayList();
		
		for (String keywordId : proposalKeywordsId) {
			for(JSONComplexTypeDTO jsonDto: allKeywordsAvailable ) {
				if (jsonDto.getId().equals(keywordId)) {
					list.add(jsonDto.getName());
				}
			}
		}
		return list;
	}

}
