package form.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RevisorDecisionListener implements TaskListener {
	
	private static final long serialVersionUID = 263868736823L;
	
	private static final String REVISOR = "revisor";
	private static final String ADVISOR_ANSWER = "decisionRevisor";
	private static final String ADVISOR_LIST = "advisorList";
	
	private static Log log = LogFactory.getLog(RevisorDecisionListener.class);
	
	@SuppressWarnings("unchecked")
	public void notify(DelegateTask delegateTask) {
		log.info("gathering advisor decision...");

		List<String> advisorAcceptedList = new ArrayList();
		
		if (delegateTask.hasVariable(ADVISOR_LIST)) {
			advisorAcceptedList = (List<String>) delegateTask.getVariable(ADVISOR_LIST);
		}
		
		String decision = delegateTask.getVariable(ADVISOR_ANSWER, String.class);
		String advisor = delegateTask.getVariable(REVISOR, String.class);
		
		String advisorDecision = advisor + ": " + decision;
		
		log.info(advisorDecision);
		
		advisorAcceptedList.add(advisorDecision);
		delegateTask.setVariable(ADVISOR_LIST, advisorDecision);
		
	}

}
