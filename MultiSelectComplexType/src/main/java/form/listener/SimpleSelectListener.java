package form.listener;

import java.io.IOException;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import dto.JSONTeacherDTO;

public class SimpleSelectListener implements TaskListener{
	private static Log log = LogFactory.getLog(SimpleSelectListener.class);
	private static final long serialVersionUID = 4322232328293829392L;
	
	private static final String SELECTED_TEACHER = "selectTeacher";
	private static final String LIST_OF_TEACHERS = "teachersRetrieved";
	private static final String TEACHER_EMAIL = "teacherEmail";
	
	public void notify(DelegateTask delegateTask) {
		log.info("PArsing available teachers...");
		
		ObjectMapper mapper = new ObjectMapper();
		if (delegateTask.getVariable(SELECTED_TEACHER).toString() != null) {
			try {
				
				String teachers = delegateTask.getVariable(LIST_OF_TEACHERS).toString();
				log.info(teachers);
				
				log.info(delegateTask.getVariable(SELECTED_TEACHER).toString());
				List<JSONTeacherDTO> listTeachers = mapper.readValue(teachers, new TypeReference<List<JSONTeacherDTO>>(){});
				
				String teacherID = delegateTask.getVariable(SELECTED_TEACHER).toString();
				
				String teacherEmail = listTeachers.get(Integer.parseInt(teacherID)-1).getEmail();
				
				delegateTask.setVariable(TEACHER_EMAIL, teacherEmail);
			}catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}

}
