package form.type;

import org.activiti.engine.form.AbstractFormType;

public class MultiSelectFormType extends AbstractFormType {
    public static final String TYPE_NAME = "multiSelect";
    
    public String getName() {
        return TYPE_NAME;
    }

    @Override
    public Object convertFormValueToModelValue(String propertyValue) {
        return propertyValue;
    }

 

    @Override
    public String convertModelValueToFormValue(Object modelValue) {
        if (modelValue == null) {
            return null;
        }
        
        return modelValue.toString();
    }
    
}
