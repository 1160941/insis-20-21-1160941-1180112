package generateProposal;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GenerateProposal implements JavaDelegate{

	private static final String TITLE = "title";
	private static final String ACCEPTED_TITLE = "titleReviewed";
	
	private static Log log = LogFactory.getLog(GenerateProposal.class);

	private static final long serialVersionUID = 132323281211829392L;
	
	public void execute(DelegateExecution arg0) throws Exception {
		log.info("Generating title...");
		//ObjectMapper mapper = new ObjectMapper();
		
		arg0.setVariable(TITLE, "TEST");
		
		
	}

}
