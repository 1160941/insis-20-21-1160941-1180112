package customProperties;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UpdateConfig implements JavaDelegate{
	private static Log log = LogFactory.getLog(UpdateConfig.class);
	
	public void execute(DelegateExecution arg0) throws Exception {
		String retry = (String) arg0.getVariable("retry");
		String timeLimit = (String) arg0.getVariable("timeLimit");
		
		log.info("old retry:" + retry);
		log.info("old timeLimit:" + timeLimit);
		if(retry.equals("true")) {
			retry = "false";
			
			Integer timeLimitInt = Integer.valueOf(timeLimit);
			timeLimit = String.valueOf(timeLimitInt/4);
			
			arg0.setVariable("retry", retry);
			arg0.setVariable("timeLimit", timeLimit);
			
			log.info("new retry:" + retry);
			log.info("new timeLimit:" + timeLimit);
			
		}

	}
	
}
